% Model reduction - homework 2

% Question 1

clear all; clc, close all; format compact; format short e;
set(0,'defaultaxesfontsize',14,'defaultlinemarkersize',8,...
   'DefaultLineLineWidth',1,'DefaultTextInterpreter','latex','defaultfigurecolor','white')
warning off
load ISS12A.mat

%% Computing the lyapchol from the matrix
sys = ss(A,B,C,D);
P_chol=lyapchol(A,B)';
Q_chol=lyapchol(A',C')';

P=P_chol'*P_chol;
Q=Q_chol'*Q_chol;

[U_P,sval_P_chol,V_P]= svd(P_chol);  % Controlability based
[U_Q,sval_Q_chol,V_Q]= svd(Q_chol);  % Observability based
[U_PQ,sval_PQ_chol,V_PQ]= svd(P_chol'*Q_chol);  % Balanced truncation


%%

S_P=(diag(sval_P_chol.^2));
S_Q=(diag(sval_Q_chol.^2));
S_PQ=(diag(sval_PQ_chol));
% gc_case1 = gram(sys,'c');

SISO_orgsys=ss(A,B,C,D);


%% Reduced model using controllability matrix r=60
% From the proof it can be understood that the left singular vector of
% P_chol becomes the eigenvectors of the orginal P matrix
figure,semilogy(S_P/S_P(1));
xlabel('Singular vectors');
ylabel('Normalized value');
title('Decay of singular value of the P matrix');
vline(60);

Ur_P=V_P(:,1:60);
Ar_P=Ur_P'*A*Ur_P;
Br_P=Ur_P'*B;
Cr_P=C*Ur_P;

size_A=size(A,1);
size_Ar=size(Ar_P,1);
err_A=zeros(size_A+size_Ar,size_A+size_Ar);
err_A(1:size_A,1:size_A)=A;

err_A(size_A+1:end,size_A+1:end)=Ar_P;
err_B=vertcat(B,Br_P);
err_C=[C -Cr_P];
err_D=D;

% SISO Error for Reduced system 1
SISO_err_redsys_P=ss(err_A,err_B,err_C,err_D);
figure,sigma(SISO_err_redsys_P);
xlim([10^-1 10^3]);
title('Sigma plot of error of reduced system - 1');

hinf_errorP=norm(SISO_err_redsys_P,'inf');
h2_errorP=norm(SISO_err_redsys_P,2);
disp(['Hinf errors for the reduced system using P =',num2str(hinf_errorP)]);
disp(['H2 errors for the reduced system using P =',num2str(h2_errorP)]);
%% Reduced model using observability matrix r=60
% From the proof it can be understood that the left singular vector of
% Q_chol becomes the eigenvectors of the orginal Q matrix

figure,semilogy(S_Q/S_Q(1));
xlabel('Singular vectors');
ylabel('Normalized value');
title('Decay of singular value of the Q matrix');
vline(60);

Ur_Q=V_Q(:,1:60);
Ar_Q=Ur_Q'*A*Ur_Q;
Br_Q=Ur_Q'*B;
Cr_Q=C*Ur_Q;


size_A=size(A,1);
size_Ar=size(Ar_Q,1);
err_A=zeros(size_A+size_Ar,size_A+size_Ar);
err_A(1:size_A,1:size_A)=A;

err_A(size_A+1:end,size_A+1:end)=Ar_Q;
err_B=vertcat(B,Br_Q);
err_C=[C -Cr_Q];
err_D=D;

% SISO Error for Reduced system 2
SISO_err_redsys_Q=ss(err_A,err_B,err_C,err_D);
figure,sigma(SISO_err_redsys_Q);
xlim([10^-1 10^3]);
title('Sigma plot of error of reduced system - 2');

hinf_errorQ=norm(SISO_err_redsys_Q,'inf');
h2_errorQ=norm(SISO_err_redsys_Q,2);
disp(['Hinf errors for the reduced system using Q =',num2str(hinf_errorQ)]);
disp(['H2 errors for the reduced system using Q =',num2str(h2_errorQ)]);
%% Reduced model using BALANCED TRUNCATION r=60
% From the proof it can be understood that the left singular vector of
% P_chol becomes the eigenvectors of the orginal P matrix
figure,semilogy(S_PQ/S_PQ(1));
xlabel('Singular vectors');
ylabel('Normalized value');
title('Decay of singular value of the hankel matrix balanced truncation case');
vline(60);

% Further steps for balanced truncation algorithm
Z1=U_PQ(:,1:60);
% Sigma_half=zeros(60,60);
Sigma_half=diag(1./sqrt(diag(sval_PQ_chol(1:60,1:60))));
Y1=V_PQ(:,1:60);

V_bt=P_chol*Z1*Sigma_half;
W_bt_tran=Sigma_half*Y1'*Q_chol';

SISO_orgsys=ss(A,B,C,D);
clear err_A err_B err_C err_D
Ar_PQ=W_bt_tran*A*V_bt;
Br_PQ=W_bt_tran*B;
Cr_PQ=C*V_bt;


size_A=size(A,1);
size_Ar=size(Ar_PQ,1);
err_A=zeros(size_A+size_Ar,size_A+size_Ar);
err_A(1:size_A,1:size_A)=A;

err_A(size_A+1:end,size_A+1:end)=Ar_PQ;
err_B=vertcat(B,Br_PQ);
err_C=[C -Cr_PQ];
err_D=D;
%%
% SISO Error for Reduced system 2
SISO_err_redsys_PQ=ss(err_A,err_B,err_C,D);
figure,sigma(SISO_err_redsys_PQ);
xlim([10^-1 10^3]);
title('Sigma plot of error of reduced system using PQ');

hinf_errorPQ=norm(SISO_err_redsys_PQ,'inf');
h2_errorPQ=norm(SISO_err_redsys_PQ,2);
disp(['Hinf errors for the reduced system using balanced truncation =',num2str(hinf_errorPQ)]);
disp(['H2 errors for the reduced system using balanced truncation =',num2str(h2_errorPQ)]);
%% Part d.

% Plotting the 2-norms

freq = logspace(-5,5,5e3);
norm2=zeros(4,length(freq));

Hr_P=zeros(3,3,length(freq));
Hr_Q=zeros(3,3,length(freq));
Hr_PQ=zeros(3,3,length(freq));
H=zeros(3,3,length(freq));
for wi = 1:length(freq)
    wi;                                                                                                                        
s=1i*freq(wi);
mat_old=[s*eye(length(A))-A];
mat_old_P = [s*eye(60)-Ar_P];
mat_old_Q = [s*eye(60)-Ar_Q];
mat_old_PQ = [s*eye(60)-Ar_PQ];

Hr_P(:,:,wi) = Cr_P*(mat_old_P\Br_P);
Hr_Q(:,:,wi) = Cr_Q*(mat_old_Q\Br_Q);
Hr_PQ(:,:,wi) = Cr_PQ*(mat_old_PQ\Br_PQ);
H(:,:,wi)=C*(mat_old\B);

norm2(1,wi)=norm(Hr_P(:,:,wi));
norm2(2,wi)=norm(Hr_Q(:,:,wi));
norm2(3,wi)=norm(Hr_PQ(:,:,wi));
norm2(4,wi)=norm(H(:,:,wi));
end


figure,loglog(freq,norm2);
xlabel('iw');
ylabel('2 - norm');
legend('P based','Q based','Balanced truncation','Original system');
title('Reduced transfer function 2 norm'); 

